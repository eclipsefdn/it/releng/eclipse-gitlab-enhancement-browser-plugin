SRC = manifest.json js icons
DIST = eclipse-gitlab-enhancement.tar.gz
ICONS = icons/icon16.png icons/icon48.png icons/icon128.png

all: $(DIST)

clean:
	rm -rf ./target$(DIST)

$(DIST): clean $(ICONS)
	mkdir -p ./target
	tar -czvf ./target/$@ $(SRC)

icons/icon%.png: icons/original.png
	convert $< -resize $*x $@

.PHONY: all clean icons
