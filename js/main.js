const API_BASE_URL = "https://api.eclipse.org/account/profile/";

async function enrichUserProfile() {
  const userLink = document.querySelector('.js-user-link');

  if (!userLink) {
    console.error("Span with class .js-user-link not found");
    return;
  }

  try {
    const userId = getUserIdFromLink(userLink.href);
    const userData = await fetchUserData(userId);
    const projectData = await fetchProjectData(userId);

    const userFormattedData = formatUserData(userData);
    const projectFormattedData = formatProjectData(projectData) || "No projects related!";

    const dataElement = createDataElement(userFormattedData, projectFormattedData);
    userLink.appendChild(dataElement);

    console.log("Eclipse user data:", userFormattedData);
    console.log("Eclipse project data:", projectFormattedData);
  } catch (error) {
    console.error("An error has occurred:", error.message);
  }
}

function getUserIdFromLink(userUrl) {
  const parts = userUrl.trim().split('/');
  return parts[parts.length - 1];
}

async function fetchDataFromApi(url) {
  const response = await fetch(url);

  if (!response.ok) {
    const errorMessage = `API error: ${response.status} - ${await response.text()}`;
    throw new Error(errorMessage);
  }

  return response.json();
}

async function fetchUserData(userId) {
  const userApiUrl = `${API_BASE_URL}${userId}`;
  return fetchDataFromApi(userApiUrl);
}

async function fetchProjectData(userId) {
  const projectApiUrl = `${API_BASE_URL}${userId}/projects`;
  return fetchDataFromApi(projectApiUrl);
}

function createDataElement(userData, projectData) {
  const dataElement = document.createElement('span');
  dataElement.setAttribute('style', 'display: flex;');
  dataElement.setAttribute('class', 'ef-description gl-display-inline gl-sm-display-none!');

  const enrichedDataElement = document.createElement('span');
  enrichedDataElement.innerHTML = userData;
  dataElement.appendChild(enrichedDataElement);

  const enrichedProjectDataElement = document.createElement('span');
  enrichedProjectDataElement.innerHTML = projectData || "No projects related!";
  dataElement.appendChild(enrichedProjectDataElement);

  return dataElement;
}

function formatUserData(data) {
  const { uid, name, eca, full_name, is_committer, org, github_handle, job_title } = data;
  return `
  <p>
    UID: ${uid}
    <br/>ECA Status: ${eca.signed}
    <br/>Name: ${name}
    <br/>Full Name: ${full_name}
    <br/>Commiter: ${is_committer}
    <br/>Organization: ${org}
    <br/>Github handle: ${github_handle}
    <br/>Job title: ${job_title}
  </p>`;
}

function formatProjectData(data) {
  let resultString = "";

  // Parcours de chaque clé du JSON
  for (const key in data) {
    // Vérification si la clé est propre à l'objet et non héritée
    if (data.hasOwnProperty(key)) {
      const entities = data[key];

      // Parcours de chaque entité pour la clé donnée
      entities.forEach((entity) => {
        const entityName = entity.ProjectName;
        const relation = entity.Relation.Description;

        resultString += `${key} (${entityName}): ${relation}<br/>`;
      });
    }
  }

  return resultString.trim();
}

function toggleClasses(elements, addClass, removeClass) {
  elements.forEach((element) => {
    element.classList.add(addClass);
    element.classList.remove(removeClass);
  });
}

if (window.location.pathname.includes('/issues/')) {
   window.onload = function () {
    enrichUserProfile();
    const userLink = document.querySelector('.js-user-link');
    userLink.addEventListener('mouseover', () => {
      const children = userLink.querySelectorAll('.ef-description');
      toggleClasses(children, 'gl-sm-display-inline', 'gl-sm-display-none!');
    });
    userLink.addEventListener('mouseout', () => {
      const children = userLink.querySelectorAll('.ef-description');
      toggleClasses(children, 'gl-sm-display-none!', 'gl-sm-display-inline');
    });
  };
}
