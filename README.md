# Eclipse GitLab Enhancement

A Chrome extension to enhance UI for `eclipse.gitlab.org`.

## Build

```shell
make
```

Zip file is generated under `./target/eclipse-gitlab-enhancement.zip``

## Installation in Chrome

* Open tab in chrome, type: `chrome://extensions/`
* Check `Developer mode`
* Click `load unpack` and navigate where you have extracted the ZIP file containing the extension.

## Firefox is not supported

Due to browser incompatiblity with attribut `ExecutionWorld`` and value `MAIN`.

* https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/scripting/ExecutionWorld
* https://bugzilla.mozilla.org/show_bug.cgi?id=1736575

